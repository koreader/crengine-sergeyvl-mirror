#!/bin/sh
TOOL=/home/sergv/toolchain/qvfb
export PATH=$TOOL/bin:$TOOL/qt4/bin:$PATH
rm -rf qtbuild_qvfb
mkdir qtbuild_qvfb
cd qtbuild_qvfb
cmake -D GUI=QT -D CR_USE_INVERT_FOR_SELECTION_MARKS=1 -D CMAKE_BUILD_TYPE=Debug -D MAX_IMAGE_SCALE_MUL=2 -D DOC_DATA_COMPRESSION_LEVEL=3 -D CR3_FREETYPE=1 -D DOC_BUFFER_SIZE=0x1400000 -D CMAKE_INSTALL_PREFIX=/usr ../..
make
